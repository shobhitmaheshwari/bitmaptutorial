package com.example.shobhit.bitmaptutorial;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

/**
 * Created by shobhit on 1/24/16.
 * Copied from Prof. Luca class code
 */
public class MyAdapter extends ArrayAdapter<ListElement> {

    int resource;
    Context context;
    Cache cache;

    public MyAdapter(Context _context, int _resource, List<ListElement> items, Cache cache) {
        super(_context, _resource, items);
        resource = _resource;
        context = _context;
        this.cache = cache;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout newView;

        ListElement w = getItem(position);

        // Inflate a new view if necessary.
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
            vi.inflate(resource,  newView, true);
        } else {
            newView = (LinearLayout) convertView;
        }
        /*
        This is to find id of images in drawable folder
         */
        int id = newView.getResources().
                getIdentifier("a" + Integer.toString(w.id), "drawable", context.getPackageName());
        ImageView mImageView = (ImageView) newView.findViewById(R.id.imageView);//get imageview of a list cell
        loadBitmap(id, mImageView);//load image into the imageview after decoding and rescaling
        return newView;
    }

    /*
    Essentially you only need to pass the resource Id and imageview for the loading to happen.
    It will happen in a separate thread because of BitmapWorkerTask.
    You may use eventhandler for getting to know when the image is loaded.
     */
    public void loadBitmap(int resId, ImageView imageView) {

        final String imageKey = String.valueOf(resId);
        final Bitmap bitmap = cache.getBitmapFromMemCache(imageKey);//first check the cache
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            //check if this particular imageview is already being used for loading a resource
            //because of a previous call.
            if (cancelPotentialWork(resId, imageView)) {
                //you will have multiple BitmapWorkerTask threads for every of the 12 images
                final BitmapWorkerTask task = new BitmapWorkerTask(imageView, context, cache);
                // mPlaceHolderBitmap is initially null and will be populated when the resource is
                //decoded. However we don't make any use of it.
                Bitmap mPlaceHolderBitmap = null;

                //Before executing the BitmapWorkerTask, you create an AsyncDrawable and bind it to
                // the target ImageView
                final AsyncDrawable asyncDrawable =
                        new AsyncDrawable(context.getResources(), mPlaceHolderBitmap, task);

                //this way you keep track of the BitmapWorkerTask associated with this imageView
                //since you can first get AsyncDrawable for imageview and from that you can retrieve
                //BitmapWorkerTask
                imageView.setImageDrawable(asyncDrawable);

                //process in a separate thread. pass the resource Id to BitmapWorkerTask
                task.execute(resId);
            }
        }
    }

    public static boolean cancelPotentialWork(int data, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = BitmapWorkerTask.getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            //this line should probably be synchronized since bitmapWorkerTask.data is changed in
            // the BitmapWorkerTask thread. However I have currently not done it.
            final int bitmapData = bitmapWorkerTask.data;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData == 0 || bitmapData != data) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }
}
