package com.example.shobhit.bitmaptutorial;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by shobhit on 2/23/16.
 * Demonstrates how to use LruCache. This will cache in memory. You may use a Disk Cache
 * but this process will be slower, however cache size will be more. Typically you will use them
 * along with LruCache so that when something is not available in the LruCache then it will be
 * taken from disk cache instead of processing again.
 *
 * You may refer http://developer.android.com/training/displaying-bitmaps/cache-bitmap.html
 * for detailed explanation.
 */
public class Cache {
    private LruCache<String, Bitmap> mMemoryCache;

    public Cache(){

        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            // Initialize cache here as usual
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }


    //add to cache
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    //retrieve from cache
    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }
}
