package com.example.shobhit.bitmaptutorial;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import java.lang.ref.WeakReference;

/**
 * Created by shobhit on 2/23/16.
 *
 * ImageView stores a reference to the most recent AsyncTask which can later be
 * checked when the task completes. This is to handle concurrency.
 *
 * You may refer http://developer.android.com/training/displaying-bitmaps/process-bitmap.html#concurrency
 * for more details.
 */
public class AsyncDrawable extends BitmapDrawable {
    private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

    public AsyncDrawable(Resources res, Bitmap bitmap,
                         BitmapWorkerTask bitmapWorkerTask) {
        super(res, bitmap);
        bitmapWorkerTaskReference =
                new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
    }

    public BitmapWorkerTask getBitmapWorkerTask() {
        return bitmapWorkerTaskReference.get();
    }
}
