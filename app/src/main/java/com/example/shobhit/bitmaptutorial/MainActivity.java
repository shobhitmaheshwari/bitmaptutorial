package com.example.shobhit.bitmaptutorial;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

/*
most of the code taken from http://developer.android.com/training/displaying-bitmaps/index.html
This app demonstrates how to load images as bitmaps in ImageView efficiently
 */
public class MainActivity extends AppCompatActivity {

    Bitmap mPlaceHolderBitmap;
    private Cache cache;
    MyAdapter aa;
    private ArrayList<ListElement> aList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        Screen orientation change, causes Android to destroy and restart the running activity with
        the new configuration (For more information about this behavior, see Handling Runtime
        Changes). You want to avoid having to process all your images again so the user has a
        smooth and fast experience when a configuration change occurs.

        The fragment will retain the cache of decoded images
        For more details refer http://developer.android.com/guide/components/fragments.html
         */
        RetainFragment retainFragment =
                RetainFragment.findOrCreateRetainFragment(getFragmentManager());
        cache = retainFragment.mRetainedCache;
        if (cache == null) {
            //This cache will be populated when the image resource gets decoded
            cache = new Cache();
            retainFragment.mRetainedCache = cache;
        }

        aList = new ArrayList<ListElement>();

        //The adapter will populate listView with images
        aa = new MyAdapter(this, R.layout.list_element, aList, cache);
        ListView myListView = (ListView) findViewById(R.id.listView);
        myListView.setAdapter(aa);
        aa.notifyDataSetChanged();//aList is empty so no change occurs now.
    }

    /*
     *  Display the images in ListView
     */
    public void display(View view){

        //I have 12 images stored locally. I want to display them in a list
        for(int i = 0; i < 12; i++){
            aList.add(new ListElement(i));
        }
        aa.notifyDataSetChanged();
    }
}
